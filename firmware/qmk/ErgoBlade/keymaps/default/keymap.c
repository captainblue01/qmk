// Copyright 2023 QMK
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H

#define _CO 0 // Colemak
#define _GA 1 // Gaming
#define _NU 2 // Numpad

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     *   ┌──┬─────┬─────┬─────┬─────────────────┐                  ┌─────────────────┬─────┬─────┬─────┬──┐
     *   │SH│BOOT │     │ 7 & │ 8 * │ 9 ( │  +  │                  │MSE 3│ HOME│ END │TABPV│TABNX│ NEXT│SH│
     *   │FT│ TAB │  Q  │  W  │  F  │  P  │  B  │                  │  J  │  L  │  U  │  Y  │ : ; │ " ' │FT│
     *   ├──┼─────┼─────┼─────┼─────┼─────┼─────┤                  ├─────┼─────┼─────┼─────┼─────┼─────┼──┤
     *   │LG│ ` ~ │  *  │ 4 $ │ 5 % │ 6 ^ │ ENT │                  │ LEFT│ DOWN│  UP │RIGHT│PG-UP│ PLAY│RG│
     *   │UI│ ESC │  A  │  R  │  S  │  T  │  G  │                  │  M  │  N  │  E  │  I  │  O  │ ENT │UI│
     *   └──┼─────┼─────┼─────┼─────┼─────┼─────┤                  ├─────┼─────┼─────┼─────┼─────┼─────┼──┤
     *      │     │  /  │ 1 ! │ 2 @ │ 3 # │  -  │                  │     │CWTOG│VOLUP│VOLDN│PG-DN│ PREV│RC│
     *      │ CTL │  Z  │  X  │  C  │  D  │  V  │                  │  K  │  H  │ < , │ > . │ / ? │ \ | │TL│
     *      └─────┴─────┼─────┼─────┼─────┴─────┘                  └─────┴─────┼─────┼─────┼─────┴─────┴──┘
     *                  │ [ { │ } ] │                                          │BR_BK│BW_FW│
     *                  │ . > │ 0 ) │                                          │ - _ │ = + │
     *                  └─────┴─────┼─────┬──────┬──────┐ ┌──────┬──────┬──────┼─────┴─────┘
     *                              │ _NU │ SPCE │ LALT │ │ RCTL │ BSPC │ SHFT │
     *                              │     │ BOOT │      │ │ _GA  │      │      │
     *                              └─────┴──────┴──────┘ └──────┴──────┴──────┘
     */
    [_CO] = LAYOUT_ortho_track(

     MT(MOD_LSFT,KC_TAB),  KC_Q, KC_W,    KC_F,    KC_P,    KC_B,                          KC_J,    KC_L,    KC_U,    KC_Y,   KC_SCLN, MT(MOD_RSFT, KC_QUOT),
     MT(MOD_LGUI, KC_ESC), KC_A, KC_R,    KC_S,    KC_T,    KC_G,                          KC_M,    KC_N,    KC_E,    KC_I,   KC_O,    MT(MOD_RGUI, KC_ENTER),
     KC_LCTL,              KC_Z, KC_X,    KC_C,    KC_D,    KC_V,                          KC_K,    KC_H,    KC_COMM, KC_DOT, KC_SLSH, MT(MOD_RCTL, KC_BSLS),
                                 KC_LBRC, KC_RBRC,                                                           KC_MINS, KC_EQL,
                                                   MO(_NU), KC_SPC, KC_LALT,      KC_RCTL, KC_BSPC, KC_RSFT
    ),
    [_GA] = LAYOUT_ortho_track(
     KC_TAB,  KC_LSFT, KC_Q,    KC_W,    KC_E,    KC_R,                           KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
     KC_TRNS, KC_ESC,  KC_A,    KC_S,    KC_D,    KC_F,                           KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
     KC_TRNS, KC_LCTL, KC_Z,    KC_X,    KC_C,    KC_V,                           KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
                       KC_TRNS, KC_TRNS,                                                          KC_TRNS,KC_TRNS,
                                         MO(_NU), KC_TRNS, KC_TRNS,      KC_TRNS, KC_TRNS, KC_TRNS
    ),
    [_NU] = LAYOUT_ortho_track(
     KC_TRNS, KC_TRNS, KC_7,   KC_8, KC_9,    KC_PPLS,                     KC_BTN3, KC_HOME, KC_END,  LCTL(KC_PGUP), LCTL(KC_PGDN), KC_MNXT,
     KC_GRV,  KC_PAST, KC_4,   KC_5, KC_6,    KC_PENT,                     KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT,       KC_PGUP,       KC_MPLY,
     KC_TRNS, KC_PSLS, KC_1,   KC_2, KC_3,    KC_PMNS,                     KC_TRNS, CW_TOGG, KC_VOLD, KC_VOLU,       KC_PGDN,       MT(MOD_RCTL, KC_MPRV),
                       KC_DOT, KC_0,                                                         KC_WBAK, KC_WFWD,
                                     KC_TRNS, QK_BOOT, KC_TRNS,   TG(_GA), KC_TRNS, KC_TRNS
    ),
};
