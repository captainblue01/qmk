// Copyright 2023 QMK
// SPDX-License-Identifier: GPL-2.0-or-later

#include QMK_KEYBOARD_H

// Auto Mouse Layer:
// void pointing_device_init_user(void) {
//     set_auto_mouse_layer(2); // only required if AUTO_MOUSE_DEFAULT_LAYER is not set to index of <mouse_layer>
//     set_auto_mouse_enable(true);         // always required before the auto mouse feature will work
// }


enum custom_keycodes {
    DRAG_SCROLL = SAFE_RANGE,
};

bool set_scrolling = false;

// Modify these values to adjust the scrolling speed
#define SCROLL_DIVISOR_H 0.0
#define SCROLL_DIVISOR_V -15.0

// Variables to store accumulated scroll values
float scroll_accumulated_h = 0;
float scroll_accumulated_v = 0;

// Function to handle mouse reports and perform drag scrolling
report_mouse_t pointing_device_task_user(report_mouse_t mouse_report) {
    // Check if drag scrolling is active
    if (set_scrolling) {
        // Calculate and accumulate scroll values based on mouse movement and divisors
        scroll_accumulated_h += (float)mouse_report.x / SCROLL_DIVISOR_H;
        scroll_accumulated_v += (float)mouse_report.y / SCROLL_DIVISOR_V;

        // Assign integer parts of accumulated scroll values to the mouse report
        mouse_report.h = (int8_t)scroll_accumulated_h;
        mouse_report.v = (int8_t)scroll_accumulated_v;

        // Update accumulated scroll values by subtracting the integer parts
        scroll_accumulated_h -= (int8_t)scroll_accumulated_h;
        scroll_accumulated_v -= (int8_t)scroll_accumulated_v;

        // Clear the X and Y values of the mouse report
        mouse_report.x = 0;
        mouse_report.y = 0;
    }
    return mouse_report;
}
uint8_t mod_state;

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

    // Store the current modifier state in the variable for later reference
    mod_state = get_mods();
    switch (keycode) {

    case KC_BSPC:
        {
        // Initialize a boolean variable that keeps track
        // of the delete key status: registered or not?
        static bool delkey_registered;
        if (record->event.pressed) {
            // Detect the activation of either shift keys
            if (mod_state & MOD_MASK_SHIFT) {
                // First temporarily canceling both shifts so that
                // shift isn't applied to the KC_DEL keycode
                del_mods(MOD_MASK_SHIFT);
                register_code(KC_DEL);
                // Update the boolean variable to reflect the status of KC_DEL
                delkey_registered = true;
                // Reapplying modifier state so that the held shift key(s)
                // still work even after having tapped the Backspace/Delete key.
                set_mods(mod_state);
                return false;
            }
        } else { // on release of KC_BSPC
            // In case KC_DEL is still being sent even after the release of KC_BSPC
            if (delkey_registered) {
                unregister_code(KC_DEL);
                delkey_registered = false;
                return false;
            }
        }
        // Let QMK process the KC_BSPC keycode as usual outside of shift
        return true;
    }

    }

    if (keycode == DRAG_SCROLL && record->event.pressed) {
        set_scrolling = !set_scrolling;
    }
    return true;
}
#define _CO 0 // Colemak
#define _GA 1 // Gaming
#define _NU 2 // Numpad

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     *                              ┌─────┐                              ┌─────┐
     *                              │ GUI │                              │ GUI │
     *      ┌─────┬─────┬─────┬─────┼─────┼─────┐                  ┌─────┼─────┼─────┬─────┬─────┬─────┐
     *      │BOOT │     │ 7 & │ 8 * │ 9 ( │  +  │                  │MSE 3│ HOME│ END │TABPV│TABNX│ NEXT│
     *      │ TAB │  Q  │  W  │  F  │  P  │  B  │                  │  J  │  L  │  U  │  Y  │ : ; │ " ' │
     *      ├─────┼─────┼─────┼─────┼─────┼─────┤                  ├─────┼─────┼─────┼─────┼─────┼─────┤
     *      │ ` ~ │  *  │ 4 $ │ 5 % │ 6 ^ │ ENT │                  │ LEFT│ DOWN│  UP │RIGHT│PG-UP│ PLAY│
     *      │ ESC │  A  │  R  │  S  │  T  │  G  │                  │  M  │  N  │  E  │  I  │  O  │ ENT │
     *      ├─────┼─────┼─────┼─────┼─────┼─────┤                  ├─────┼─────┼─────┼─────┼─────┼─────┼──┐
     *      │     │  /  │ 1 ! │ 2 @ │ 3 # │  -  │                  │     │CWTOG│VOLUP│VOLDN│PG-DN│ PREV│CT│
     *      │ CTL │  Z  │  X  │  C  │  D  │  V  │                  │  K  │  H  │ < , │ > . │ / ? │ \ | │RL│
     *      └─────┴─────┼─────┼─────┼─────┴─────┘                  └─────┴─────┼─────┼─────┼─────┴─────┴──┘
     *                  │ [ { │ } ] │                                          │BR_BK│BW_FW│
     *                  │ . > │ 0 ) │                                ┌─────┐   │ - _ │ = + │
     *                  └─────┴─────┼───┬───┐                        │SHIFT│   └─────┴─────┘
     *                              │RSE│SPC│                        └─────┘
     *                              └───┴───┼───┬───┐                  ┌───┐
     *                                      │MS1│MS2│                  │BSP│
     *                                      ├───┼───┤            ┌─────┼───┤
     *                                      │ALT│   │            │DRGSL│CTL│
     *                                      └───┴───┘            └─────┴───┘
     */
    [_CO] = LAYOUT_ortho_track(
     KC_TRNS,              KC_TRNS,  KC_TRNS,    KC_TRNS,    KC_LGUI, KC_TRNS,          KC_TRNS,    KC_RGUI,    TG(_GA),    KC_TRNS,      KC_TRNS,       KC_TRNS,
     MT(MOD_LSFT,KC_TAB),  KC_Q,     KC_W,       KC_F,       KC_P,    KC_B,             KC_J,       KC_L,       KC_U,       KC_Y,         KC_SCLN,       MT(MOD_RSFT, KC_QUOT),
     MT(MOD_LGUI, KC_ESC), KC_A,     KC_R,       KC_S,       KC_T,    KC_G,             KC_M,       KC_N,       KC_E,       KC_I,         KC_O,          MT(MOD_RGUI, KC_ENTER),
     KC_LCTL,              KC_Z,     KC_X,       KC_C,       KC_D,    KC_V,             KC_K,       KC_H,       KC_COMM,    KC_DOT,       KC_SLSH,       MT(MOD_RCTL, KC_BSLS),
                                     KC_LBRC,    KC_RBRC,                                                       KC_MINS,    KC_EQL,
                                                 MO(_NU), KC_SPC, KC_BTN1, KC_BTN2,             DRAG_SCROLL, KC_RALT, KC_BSPC,KC_RSFT
    ),
    [_GA] = LAYOUT_ortho_track(
     KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,                               KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
     KC_TAB,KC_LSFT,KC_Q,   KC_W,   KC_E,   KC_R,                                  KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
     KC_TRNS,KC_ESC, KC_A,   KC_S,   KC_D,   KC_F,                                  KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
     KC_TRNS,KC_LCTL,KC_Z,   KC_X,   KC_C,   KC_V,                                  KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS,
                     KC_TRNS,KC_TRNS,                                                                  KC_TRNS,KC_TRNS,
                                     MO(_NU),  KC_TRNS,KC_TRNS,KC_TRNS,  KC_TRNS,KC_TRNS,KC_TRNS,KC_TRNS
    ),

    [_NU] = LAYOUT_ortho_track(
     KC_TRNS,   KC_TRNS, KC_TRNS,    KC_TRNS,    KC_TRNS, KC_TRNS,                   KC_WREF,    KC_TRNS,     KC_TRNS,  KC_TRNS,       KC_TRNS,       KC_TRNS,
     KC_TRNS,   KC_TRNS, KC_7,       KC_8,       KC_9,    KC_PPLS,                   KC_BTN3,    KC_HOME,     KC_END,   LCTL(KC_PGUP), LCTL(KC_PGDN), KC_MNXT,
     KC_GRV,    KC_PAST, KC_4,       KC_5,       KC_6,    KC_PENT,                   KC_LEFT,    KC_DOWN,     KC_UP,    KC_RGHT,       KC_PGUP,       KC_MPLY,
     KC_TRNS,   KC_PSLS, KC_1,       KC_2,       KC_3,    KC_PMNS,                   KC_TRNS,    CW_TOGG,     KC_VOLD,  KC_VOLU,       KC_PGDN,       MT(MOD_RCTL, KC_MPRV),
                         KC_DOT,     KC_0,                                                                    KC_WBAK,  KC_WFWD,
                                     KC_TRNS, QK_BOOT, KC_TRNS, KC_TRNS,    TG(_GA),KC_TRNS,    KC_TRNS,     KC_TRNS
    )
};
