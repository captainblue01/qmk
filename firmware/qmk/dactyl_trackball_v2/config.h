// Copyright 2023 BrodieH (@BrodieH)
// SPDX-License-Identifier: GPL-2.0-or-later

#pragma once
#define MASTER_RIGHT
#define SPLIT_USB_DETECT
#define SPLIT_USB_TIMEOUT 2000

#define USE_SERIAL

#define ROTATIONAL_TRANSFORM_ANGLE  -120
#define POINTING_DEVICE_INVERT_Y
// #define POINTING_DEVICE_RIGHT
#define PMW33XX_CS_PIN B6
#define POINTING_DEVICE_TASK_THROTTLE_MS 1
#define PMW33XX_LIFTOFF_DISTANCE 4.9
#define PMW33XX_CPI 400

#define SPLIT_TRANSACTION_IDS_KB RPC_ID_KB_CONFIG_SYNC
// This specifies the customized communication handler which handles the extra trackball modes.
// #define CHARYBDIS_MINIMUM_DEFAULT_DPI 1200
// #define CHARYBDIS_DEFAULT_DPI_CONFIG_STEP 400
// #define CHARYBDIS_MINIMUM_SNIPING_DPI 400
// #define CHARYBDIS_SNIPING_DPI_CONFIG_STEP 200
// The defaults for the sensor's sensitivity for the various modes.
#define CHARYBDIS_POINTER_ACCELERATION_ENABLE
// This will apply some acceleration calculations to the trackball's motion in addition to whatever pointer acceleration you have set up in your OS. You can comment this out if you don't need it.

// Auto Mouse Layers
// #define POINTING_DEVICE_AUTO_MOUSE_ENABLo
// #define PERMISSIVE_HOLD
#define HOLD_ON_OTHER_KEY_PRESS_PER_KEY
#define TAPPING_TERM 200
#define TAPPING_TERM_PER_KEY
