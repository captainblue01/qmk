# My Keyboards

This is my repo for my custom keyboards, I have use Dactyl Trackball keyboards for my two desktop systems, and a custom low profile wireless keyboard with my laptop

## Dactyl Trackball

### Version 1

![Version 1 Dactyl Trackball](https://i.imgur.com/HJhVZHP.jpeg)
![Version 1 Dactyl Trackball](https://i.imgur.com/7ZygJDb.jpeg)
[Firmware](./firmware/qmk/dactyl_trackball_v1/)

### Version 2

![Version 2 Dactyl Trackball](https://i.imgur.com/ox6q9ay.jpeg)
![Version 2 Dactyl Trackball](https://i.imgur.com/qkYw0z9.jpeg)
[Firmware](./firmware/qmk/dactyl_trackball_v2/)

### Version 3

[Firmware](./firmware/qmk/dactyl_trackball_v3/)

## Ergo Blade (bluetooth)

### Version 1

![Version 1 Ergo Blade](https://i.imgur.com/6lLIOLm.jpeg)
[Firmware QMK](./firmware/qmk/dactyl-trackball-v2/)
[Firmware ZMK](./firmware/zmk/)

## Wiring

All of these keyboards are handwired. The trackball I am using a PMW3360 and [this breakout board](https://github.com/Ariamelon/Ogen) I ordered the breakout boad from JLCPCB.
![Dactyl V2 Wiring Right](https://i.imgur.com/fiuBmd4.jpeg)
![Dactyl V1 Wiring Left](https://i.imgur.com/SBr7xrn.jpeg)
![Ergo Blade Wiring](https://i.imgur.com/nJWnW2k.jpeg)

## Links

### PMW3360 breakout board

[https://github.com/Ariamelon/Ogen](https://github.com/Ariamelon/Ogen)

### Dactyl Trackball Generator

[Wylderbuilds Dactyl Manuform](https://github.com/bullwinkle3000/dactyl-keyboard)

### Ergogen

[Ergogen](https://github.com/ergogen/ergogen)

### Guides

[Dactyl Manuform R Track](https://github.com/Schievel1/dactyl_manuform_r_track)
<br>
[Wylder Builds](https://wylderbuilds.com/building-it)
